FROM python:3.8-alpine

ENV FLASK_APP=api

RUN apk add build-base
COPY . .
RUN pip install -r requirements.txt

CMD ["python", "-m", "flask", "run"]