import unittest
import requests

class TestMovies(unittest.TestCase):
    URL = 'http://127.0.0.1:5000/'
    
    INSERT_DATA = {'title': 'Movie 1', 'rating': 10}
    UPDATED_DATA = {'title': 'Movie 2', 'rating': 9}

    def test_get(self):
        response = requests.get(self.URL + '/movies')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers['content-type'], 'application/json')
        self.assertTrue(b'movies' in response.content)

    def test_post(self):
        response = requests.post(self.URL + '/movies', json=self.INSERT_DATA)

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.headers['content-type'], 'application/json')
        self.assertDictContainsSubset(self.INSERT_DATA, response.json()['movie'])

        requests.delete(self.URL + '/movies' +  str(response.json()['movie']['id']))
       

    def test_put(self):
        # POST DATA IN ORDER TO BE ABLE TO CHANGE IT AFTERWARDS
        post_id = requests.post(self.URL + '/movies', json=self.INSERT_DATA).json()['movie']['id']

        response = requests.put(self.URL + '/movies/' + str(post_id), json=self.UPDATED_DATA)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers['content-type'], 'application/json')
        self.assertDictContainsSubset(self.UPDATED_DATA, response.json()['movie'])

        # REMOVE TEST DATA
        requests.delete(self.URL + '/movies' +  str(response.json()['movie']['id']))


    def test_delete(self):
        # POST DATA IN ORDER TO BE ABLE TO CHANGE IT AFTERWARDS
        post_id = requests.post(self.URL + '/movies', json=self.INSERT_DATA).json()['movie']['id']

        response = requests.delete(self.URL + '/movies/' + str(post_id))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers['content-type'], 'application/json')
        self.assertDictEqual({'success': True}, response.json())