from flask import Blueprint, jsonify, request
from . import db
from .models import Movie
import json

movies = Blueprint('movies', __name__)

# GET MOVIES
@movies.route('/movies', methods=['GET'])
def get_movies():
    try:
        movie_list = Movie.query.all()
        
        json_movie_list = []

        for movie in movie_list: 
            json_movie_list.append({'id': movie.id, 'title': movie.title, 'rating': movie.rating})

        return jsonify({'success': True, 'movies': json_movie_list}), 200
    except Exception as e:
        return jsonify({'success': False, 'error': e.description}), e.code


# CREATE MOVIE    
@movies.route('/movies', methods=['POST'])
def create_movie():
    try:
        movie_data = request.get_json()

        new_movie = Movie(title=movie_data['title'], rating=movie_data['rating'])

        db.session.add(new_movie)
        db.session.commit()

        return jsonify({'success': True, 'movie': {'id': new_movie.id, 'title' : new_movie.title, 'rating': new_movie.rating}}), 201
    except Exception as e:
        return jsonify({'success': False, 'error': e.description}), e.code


# UPDATE MOVIE  
@movies.route('/movies/<id>', methods=['PUT'])
def update_movies(id):
    try:
        movie_data = request.get_json()

        movie = Movie.query.filter_by(id=id).first_or_404()

        movie.title = movie_data['title']
        movie.rating = movie_data['rating']

        db.session.add(movie)
        db.session.commit()

        return jsonify({'success': True, 'movie': {'id': movie.id, 'title' : movie.title, 'rating': movie.rating}}), 200
    except Exception as e:
        return jsonify({'success': False, 'error': e.description}), e.code


# DELETE MOVIE
@movies.route('/movies/<id>', methods=['DELETE'])
def delete_movie(id):
    try:
        movie = Movie.query.filter_by(id=id).first_or_404()

        db.session.delete(movie)
        db.session.commit()

        return jsonify({'success': True}), 200
    except Exception as e:
        return jsonify({'success': False, 'error': e.description}), e.code
